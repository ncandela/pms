# -*- coding: utf-8 -*-
# Copyright (c) 2019, Oasis and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe import _
from frappe.model.document import Document
from erpnext.controllers.queries import item_query

class RestauranteOrden(Document):
	pass


def item_query_restaurant(doctype='Item', txt='', searchfield='name', start=0, page_len=20, filters=None, as_dict=False):
	'''Return items that are selected in active menu of the restaurant'''

	//items = frappe.db.get_all('Restaurante Producto Menu', ['producto'], dict(parent = ))
	del filters['table']
	filters['name'] = ('in', [d.item for d in items])

	return item_query('Item', txt, searchfield, start, page_len, filters, as_dict)
