// Copyright (c) 2019, Oasis and contributors
// For license information, please see license.txt

/*
frappe.ui.form.on('Restaurante', {

	setup: function(frm) {
		frm.set_query('menu_actual', function() {
			return {
				"filters" {
					"restaurante" : frm.docname,
				}
			}
		});
	}


});
*/

frappe.ui.form.on('Restaurante', {
	complejo: function(frm) {
		frm.set_query('hotel', function() {
				return {
					"filters": {
						"complejo":frm.doc.complejo,
					}
				}
			});
		},

		refresh: function(frm) {

			frm.add_custom_button(__('Nueva reserva'), () => {
				return frm.trigger('nueva_reserva');
			});
		},

		nueva_reserva: function(frm) {
			//Crear nuevo doctype en Reservas
			// LLamar a la pantalla con la nueva reserva
		}

});
