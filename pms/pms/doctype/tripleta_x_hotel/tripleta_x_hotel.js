// Copyright (c) 2019, Oasis and contributors
// For license information, please see license.txt

frappe.ui.form.on('Tripleta x Hotel', {
	tripleta: function(frm) {
		console.log('Tripleta');
/*
		var trp = frappe.get_doc('Tripleta', cur_frm.doc.tripleta);
		console.log(trp);


		cur_frm.doc.mercado = trp.mercado;
		cur_frm.doc.canal = trp.canal;
		cur_frm.doc.origen = trp.origen;
		cur_frm.doc.bloqueada = trp.bloqueada;
*/
		trp = frappe.call({
						method: "frappe.client.get",
						args: {
								doctype: "Tripleta",
								name: cur_frm.doc.tripleta,
						},
						callback(r) {
								if(r.message) {
										var doc_trip = r.message;
										console.log(doc_trip);
										console.log(doc_trip.mercado);
										cur_frm.set_value('mercado',doc_trip.mercado);
										cur_frm.set_value('canal',doc_trip.canal);
										cur_frm.set_value('origen',doc_trip.origen);
										cur_frm.set_value('bloqueada',doc_trip.bloqueada);


								}
						}
				});


	}
});
