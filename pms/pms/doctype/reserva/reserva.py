# -*- coding: utf-8 -*-
# Copyright (c) 2019, Oasis and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
from datetime import timedelta
import frappe
import pdb
from frappe.model.document import Document



class Reserva(Document):
    def validate(self):

		if not self.fecha_salida:
			self.fecha_salida = frappe.utils.add_days(self.fecha_llegada,self.noches)

		if not self.regimen_factura:
			self.regimen_factura = self.regimen_uso

		if not self.tipo_habitacion_factura:
			self.tipo_habitacion_factura = self.tipo_habitacion_uso

        
