// Copyright (c) 2019, Oasis and contributors
// For license information, please see license.txt

// Globales
var obj_hotel;
var obj_tripletaxhotel;
var obj_tripleta;

frappe.ui.form.on('Reserva', {
	onload: function(frm) {
		frm.fields_dict['valoracion'].collapse();
		frm.fields_dict['club'].collapse();
		frm.fields_dict['section_break_38'].collapse();
		frm.fields_dict['seccion_grupo'].collapse();

	}
});

function calcula_fecha_salida(frm){
	console.log('Calcula fecha salida');
	console.log(cur_frm.doc.fecha_salida = null);
	if (cur_frm.doc.fecha_salida) {
	//	dias = frappe.datetime.get_day_diff(cur_frm.doc.fecha_salida, cur_frm.doc.fecha_llegada);
		frm.set_value('noches',frappe.datetime.get_day_diff(frm.doc.fecha_salida, frm.doc.fecha_llegada));
	} else {
	//	fecha = frappe.datetime.add_days(cur_frm.doc.fecha_llegada,cur_frm.doc.noches);
	//	console.log(fecha);
		frm.set_value('fecha_salida',frappe.datetime.add_days(frm.doc.fecha_llegada,frm.doc.noches));



	};
}


function selecciona_tripleta(frm) {
	console.log('Selecciona tripleta');
	frm.set_value('tripleta',''); //Borro la seleccion que hay en la tripleta
	frm.set_query('tripleta', function() {
		return {
			"filters": {
				"hotel":frm.doc.hotel,
				"tipo":frm.doc.tipo,
				"bloqueada": false
			}
		}
	});
}

function carga_valores_defecto(frm){
	frappe.call({
					method: "frappe.client.get",
					args: {
							doctype: "Hotel",
							name: frm.doc.hotel,
					},
					callback(r) {
							if(r.message) {
									obj_hotel = r.message;
									frm.set_value('regimen_uso',obj_hotel.regimen);
									frm.set_value('trato',obj_hotel.trato);
									frm.set_value('hora_llegada',obj_hotel.hora_llegada);
									frm.set_value('hora_salida',obj_hotel.hora_salida);
							}
					}
			});
}

function carga_tripletas(frm) {
		console.log('Carga tripletas');

		// Buscamo en tripletas x hotel
		frappe.call({
						method: "frappe.client.get",
						args: {
								doctype: "Tripleta x Hotel",
								name: frm.doc.tripleta,
						},
						callback(r) {
								if(r.message) {
										console.log('Tripleta x hotel encontrada');
										obj_tripletaxhotel = r.message;
										console.log('Tripleta x hotel asignada a obj_tripletaxhotel')
										frm.set_value('mercado',obj_tripletaxhotel.mercado);
										frm.set_value('canal',obj_tripletaxhotel.canal);
										frm.set_value('origen',obj_tripletaxhotel.origen);

										/// HACEMOS LA BUSQUEDA DE TRIPLETA
										console.log('codigo tripleta:' + obj_tripletaxhotel.tripleta);

										frappe.call({
														method: "frappe.client.get",
														args: {
																doctype: "Tripleta",
																name: obj_tripletaxhotel.tripleta,
														},
														callback(r) {
																if(r.message) {
																		console.log('Tripleta encontrada');
																		obj_tripleta = r.message;
																		console.log('Tripleta asignada a obj_tripleta');

																		frm.set_df_property("bono", "reqd",obj_tripleta.bono_obligatorio);
																}
														}
												});



								}
						}
				});

		};




//Filtra selecciones dependiendo del Hotel

frappe.ui.form.on('Reserva', {
	hotel: function(frm) {
		console.log('Hotel filtro');
		if (frm.doc.hotel) {
			selecciona_tripleta(frm);
			carga_valores_defecto(frm);
		};

	}
});

frappe.ui.form.on('Reserva', {
	tipo: function(frm) {
		console.log('Tipo filtro');
		selecciona_tripleta(frm);
	}
});


frappe.ui.form.on('Reserva', {
	tripleta: function(frm) {
			if (frm.doc.tripleta) {
				carga_tripletas(frm);
			};
		}
});




//Validaciones en los campos

frappe.ui.form.on('Reserva', {
	fecha_llegada: function(frm) {
		if (frm.doc.fecha_llegada < frm.doc.fecha_venta) {
	    frappe.msgprint(__("Fecha de llegada debe ser mayor o igual a la fecha de venta."));
			frm.set_value('fecha_llegada',frm.doc.fecha_venta);
			validated = false;
		}


		calcula_fecha_salida(frm);
	}
});

frappe.ui.form.on('Reserva', {
	noches: function(frm) {
		if (frm.doc.noches < 1) {
      frappe.msgprint(__("Las noches no pueden ser menor a una noche."));
			frm.set_value('noches',1);
			frappe.validated = false;
		}

		calcula_fecha_salida(frm);

	}
});
