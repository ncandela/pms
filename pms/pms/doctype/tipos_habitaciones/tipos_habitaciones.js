// Copyright (c) 2019, Oasis and contributors
// For license information, please see license.txt

// NCA Creo el campo titulo para no repetir tipos de habitacion x hotel
frappe.ui.form.on('Tipos Habitaciones', {
	validate: function(frm) {
			frm.set_value("titulo",frm.doc.hotel + "-" + frm.doc.tipo);
	}
});
