"""
Configuration for docs
"""

source_link = "https://github.com/oasis/pms"
docs_base_url = "https://oasis.github.io/pms"
headline = "PMS Oasis"
sub_heading = "YProperty Managment System"

def get_context(context):
	context.brand_html = "PMS"
